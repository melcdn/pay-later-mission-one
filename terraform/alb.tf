# Create the ALB
resource "aws_lb" "alb" {
  name               = "${var.client_name}-alb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.alb_sg.id]
  subnets            = aws_subnet.public_subnet.*.id
  tags = {
    tags = "${var.client_name}-alb"
  }
}
# Create Apache target group
resource "aws_lb_target_group" "apache_tg" {
  name        = "${var.client_name}-apache-tg"
  port        = 80
  protocol    = "HTTP"
  target_type = "instance"
  vpc_id      = aws_vpc.main.id

  health_check {
    port = 80
    path = "/apache/index.html"
  }
}
# Create Nginx target group
resource "aws_lb_target_group" "nginx_tg" {
  name        = "${var.client_name}-nginx-tg"
  port        = 80
  protocol    = "HTTP"
  target_type = "instance"
  vpc_id      = aws_vpc.main.id

  health_check {
    port = 80
    path = "/nginx/index.html"
  }
}
# Default route is Nginx target group if path is not specified
resource "aws_lb_listener" "alb_listener" {
  load_balancer_arn = aws_lb.alb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.nginx_tg.arn
  }
}
# Route to Apache target group if path is /apache*
resource "aws_lb_listener_rule" "apache_rule" {
  listener_arn = aws_lb_listener.alb_listener.arn

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.apache_tg.arn
  }

  condition {
    path_pattern {
      values = ["/apache*"]
    }
  }
}

# Route to Nginx target group if path is /nginx*
resource "aws_lb_listener_rule" "nginx_rule" {
  listener_arn = aws_lb_listener.alb_listener.arn
  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.nginx_tg.arn
  }

  condition {
    path_pattern {
      values = ["/nginx*"]
    }
  }
}