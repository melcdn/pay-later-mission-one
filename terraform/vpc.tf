# Create Virtual Private Cloud
resource "aws_vpc" "main" {
  cidr_block           = var.vpc_cidr
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Name = "${var.client_name}-vpc"
  }
}

# Create Internet Gateway
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "${var.client_name}-igw"
  }
}

# Create Public Route Table
resource "aws_route_table" "public_rt" {
  vpc_id = aws_vpc.main.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }

  tags = {
    Name = "${var.client_name}-public-rt"
  }
}

# Create Private Route Table
resource "aws_default_route_table" "private_rt" {
  default_route_table_id = aws_vpc.main.default_route_table_id

  route {
    cidr_block  = "0.0.0.0/0"
    instance_id = aws_instance.bastion.id
  }

  tags = {
    Name = "${var.client_name}-private-rt"
  }
}

# Create Public Subnets 1 and 2
resource "aws_subnet" "public_subnet" {
  count                   = 2
  cidr_block              = var.public_subnet_cidrs[count.index]
  vpc_id                  = aws_vpc.main.id
  map_public_ip_on_launch = true
  availability_zone       = data.aws_availability_zones.available_azs.names[count.index]

  tags = {
    Name = "${var.client_name}-public-subnet-${count.index + 1}"
  }
}

# Create Private Subnets 1 and 2
resource "aws_subnet" "private_subnet" {
  count             = 2
  cidr_block        = var.private_subnet_cidrs[count.index]
  vpc_id            = aws_vpc.main.id
  availability_zone = data.aws_availability_zones.available_azs.names[count.index]

  tags = {
    Name = "${var.client_name}-private-subnet-${count.index + 1}"
  }
}

# Create Associate Public Subnet with Public Route Table
resource "aws_route_table_association" "public_subnet_assoc" {
  count          = 2
  route_table_id = aws_route_table.public_rt.id
  subnet_id      = aws_subnet.public_subnet.*.id[count.index]
  depends_on     = [aws_route_table.public_rt, aws_subnet.public_subnet]
}

# Create Associate Private Subnet with Private Route Table
resource "aws_route_table_association" "private_rt" {
  count          = 2
  route_table_id = aws_default_route_table.private_rt.id
  subnet_id      = aws_subnet.private_subnet.*.id[count.index]
  depends_on     = [aws_default_route_table.private_rt, aws_subnet.private_subnet]
}