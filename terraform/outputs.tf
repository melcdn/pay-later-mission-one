
output "region" {
  value = data.aws_region.current.name
}

output "available_azs" {
  value = data.aws_availability_zones.available_azs.names
}
output "my_public_ip" {
  value = local.my_public_ip
}

output "az0" {
  value = element(data.aws_availability_zones.available_azs.names, 1)
}
output "az1" {
  value = element(data.aws_availability_zones.available_azs.names, 0)
}
output "amazon_linux_2_latest" {
  value = data.aws_ami.amazon_linux2_latest.id
}

output "amazon_linux_nat_latest" {
  value = data.aws_ami.amazon_linux_nat_latest.id
}

output "ec2_fleet_tagging_role" {
  value = data.aws_iam_role.ec2_fleet_tagging_role.name
}

output "bastion_host_public_ip" {
  value = aws_instance.bastion.public_ip
}

output "postgresql_main_endpoint" {
  value = aws_db_instance.rds_main.endpoint
}

output "postgresql_standby_endpoint" {
  value = aws_db_instance.rds_standby.endpoint
}

output "alb_dns_name" {
  value = aws_lb.alb.dns_name
}