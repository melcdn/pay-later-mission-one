# VPC Random Sample
---
## **VPC Details**
---
## AWS Cloud Diagram
![AWS Cloud Diagram](/images/aws-diagram.png)
## VPC
| CIDR      | `10.0.0.0/16` |
| ----------| ----------- |
| Region       | `{selected-region}`  
| Total IP     | `32 - 16 = 16 \| 2^16 = 65,536 IPs` |  

## SUBNETS
* ### `Public Subnet A`
    | CIDR      | `10.0.0.0/24` |
    | ----------| ----------- |
    | Total IP     | `32 - 24 = 8 \| 2^8 = 256 IPs` |
    | Network IP   | `10.0.0.0` |
    | Broadcast IP | `10.0.0.255` |
    | Usable IP    | `10.0.0.1 - 10.0.0.254` |
    | Availability Zone | `{region-az1}`|
    ---
* ### `Public Subnet B`
    | CIDR      | `10.0.2.0/24` |
    | ----------| ----------- |
    | Total IP     | `32 - 24 = 8 \| 2^8 = 256 IPs` |
    | Network IP   | `10.0.2.0` |
    | Broadcast IP | `10.0.2.255` |
    | Usable IP    | `10.0.2.1 - 10.0.0.254` |
    | Availability Zone | `{region-az2}`|
    ---
* ### `Private Subnet A`
    | CIDR      | `10.0.1.0/24` |
    | ----------| ----------- |
    | Total IP     | `32 - 24 = 8 \| 2^8 = 256 IPs` |
    | Network IP   | `10.0.1.0` |
    | Broadcast IP | `10.0.1.255` |
    | Usable IP    | `10.0.1.1 - 10.0.0.254` |
    | Availability Zone | `{region-az1}`|
    ---
* ### `Private Subnet B`
    | CIDR      | `10.0.3.0/24` |
    | ----------| ----------- |
    | Total IP     | `32 - 24 = 8 \| 2^8 = 256 IPs` |
    | Network IP   | `10.0.3.0` |
    | Broadcast IP | `10.0.3.255` |
    | Usable IP    | `10.0.3.1 - 10.0.0.254` |
    | Availability Zone | `{region-az1}`|
---
## **Implementation**
---
### **AWS Account**
1. Open your AWS console and take note of the `region` where you want to deploy this `stack`. 
    > Example. `us-west-1`
2. Get your `AWS_ACCESS_KEY_ID` and `AWS_ACCESS_SECRET_KEY` from your AWS account.

    > Guide: [How to create AWS access keys](https://aws.amazon.com/premiumsupport/knowledge-center/create-access-key/).
3. Get (*or create if not existing*) the name of your key pair from the selected region.
    > Note: `key-pairs` are region-based.

    > Guide: [How to create key pairs](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-key-pairs.html#prepare-key-pair).
---
## **Docker Setup**
---
### **On Windows:**
1. Install [docker-compose](https://docs.docker.com/compose/install/).

    Verify `docker-compose` installation:
    ```bash
    docker-compose --version
    ```

### **Provision AWS Stacks**
1. Clone this repository using [Git](https://git-scm.com/).
    ```bash
    git clone https://gitlab.com/melcdn/pay-later-mission-one.git
    ```
2. Change to the `working directory`.

    ```bash
    cd pay-later-mission-one
    ```
3. Download required `aws provider plugins` using `terraform init`.
    ```bash
    docker-compose run --rm tf init
    ```
    > From this point:
    > * Make sure you have the required permissions on your AWS account to execute stack creation and deletion.
    > * For `steps 4, 5` and [Cleanup](#cleanup), change arguments with `{}` data  you obtained from the previous steps.
    > * Replace them with corresponding values without the `{}`.
    > * example: `-var="key_pair_name={key-pair-name}"` to `-var="key_pair_name=mel-key-pair"`.
4. Run `terraform plan` and review the changes you about to commit on your AWS account (remove new lines).
    ```bash
    docker-compose run
        -e AWS_ACCESS_KEY_ID={YOUR_AWS_ACCESS_KEY_ID}
        -e AWS_SECRET_ACCESS_KEY={AWS_SECRET_ACCESS_KEY}
        --rm tf plan
        -var="key_pair_name={key-pair-name}"
        -var="region={selected-region}"
    ```
5. Run the `terraform apply -auto-approve` to commit the changes to your AWS account (remove new lines).
    ```bash
    docker-compose run
        -e AWS_ACCESS_KEY_ID={YOUR_AWS_ACCESS_KEY_ID}
        -e AWS_SECRET_ACCESS_KEY={AWS_SECRET_ACCESS_KEY}
        --rm tf apply
        -var="key_pair_name={key-pair-name}"
        -var="region={selected-region}"
        -auto-approve
    ```
    > If properly configured, you should see something like below image:

    ![Expected Apply Result](/images/tf-apply.png)

    > If successful, you should see something like below image with `34 resources` added.

    > Take note of values as we will be using these on [Validation](#validation).

    ![Expected Approve Result](/images/tf-apply-success.png)

    
### **Validation**
---
#### **Application Load Balancer**
1. Refer to the `output` of `terraform apply` command. Copy the value of `alb_dns_name`.
2. Paste it on your browser and `/apache` at the end. If you hit `refresh` from time to time. You see that there are `one web server` from each `private subnets` are serving this path.
    ![ALB Apache](/images/alb-apache.png)

3. Repeat previous step but use `/nginx` instead of `/apache`.
    ![ALB Nginx](/images/alb-nginx.png)

#### **SSH to Bastion**
1. Refer to the `output` of `terraform apply` command. Copy the value of `bastion_host_public_ip`.
2. Open your terminal and follow below steps to remote your `bastion host`.
    ```bash
    ssh-add "c:\path\to\your\key-pair.pem"
    ```
    > Pass the actual path to your key-pair.
    ```bash
    ssh -A "c:\path\to\your\key-pair.pem"
    ```
    > The -A option is to forward agent authentication. So you can remote the `web servers` from the `bastion server` without passing the keys. View more on [SSH commands](https://www.ssh.com/academy/ssh/command).
    ```bash
    ssh ec2-user@{bastion_host_public_ip}
    ```
    > If you pass the correct IP address you will see something like below.

    ![SSH Bastion](/images/ssh-bastion.png)

#### **Database Ports**
1. Refer to the [Application Load Balancer](#application-load-balancer) images and take note the four private IP address shown.
2. Refer to the `output` of `terraform apply` command. Take note the values of `postgresql_main_endpoint` and `postgresql_standby_endpoint`.
3. While still on `bastion` hosts terminal,`SSH` to one of the `private IP` you got from `Step 1`.
    ```bash
    ssh ec2-user@{private-ip}
    ```
4. Test the connection to the `database port` using `telnet`.
    ```bash
    telnet {postgresql_main_endpoint} 5432
    telnet {postgresql_stanby_endpoint} 5432
    ```
    > **Do not forget to disconnect** to avoid problems during the [Cleanup](#cleanup).
    
    > `CTRL+C` to disconnect    

    > Note: Please observe the separation of `instance name` and `port number`.

    ![DB Port](/images/db-port.png)
5. Repeat steps using the other three private IP addresses.

#### **Cleanup**
1. Remove everything you deployed using `terraform destroy`.
    ```bash
    docker-compose run
        -e AWS_ACCESS_KEY_ID={YOUR_AWS_ACCESS_KEY_ID}
        -e AWS_SECRET_ACCESS_KEY={AWS_SECRET_ACCESS_KEY}
        --rm tf destroy
        -var="key_pair_name={key-pair-name}"
        -var="region={selected-region}"
        -auto-approve
    ```    
    > If successful, you should see something like below image with `34 resources` destroyed.

    > It should be the same with the number of resources add on the `terraform apply` command.

    ![Expected Destroy Result](/images/tf-destroy-success.png)
---
