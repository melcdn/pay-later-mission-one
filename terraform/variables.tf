variable "client_name" {
  default = "xyz"
}

variable "key_pair_name" {
  type = string
}

variable "region" {
  type = string
}

variable "instance_type" {
  default = "t2.micro"
}

variable "bastion_user_data_location" {
  default = "ec2_user_data/bastion.txt"
}

variable "apache_user_data_location" {
  default = "ec2_user_data/apache.txt"
}

variable "nginx_user_data_location" {
  default = "ec2_user_data/nginx.txt"
}


variable "vpc_cidr" {
  default = "10.0.0.0/16"
}

variable "public_subnet_cidrs" {
  type    = list(string)
  default = ["10.0.0.0/24", "10.0.2.0/24"]
}

variable "private_subnet_cidrs" {
  type    = list(string)
  default = ["10.0.1.0/24", "10.0.3.0/24"]
}

