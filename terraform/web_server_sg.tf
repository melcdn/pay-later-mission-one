# Security Group Creation
resource "aws_security_group" "web_server_sg" {
  name   = "${var.client_name}-web-server-sg"
  vpc_id = aws_vpc.main.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [var.public_subnet_cidrs[0]]
    description = "Allow SSH (22) remote from public-subnet-1 (bastion host)"
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Allow HTTP from anywhere"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
    description = "Allow all trafic"
  }

  tags = {
    Name = "${var.client_name}-web-server-sg"
  }
}