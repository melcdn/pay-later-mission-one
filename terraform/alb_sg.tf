# Create the security group of the ALB
resource "aws_security_group" "alb_sg" {
  name   = "${var.client_name}-alb-sg"
  vpc_id = aws_vpc.main.id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Allow 80 from anywhere"
  }

  egress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = var.private_subnet_cidrs
    description = "Allow ALB to access web servers (80) on 2 private subnets"
  }

  tags = {
    Name = "${var.client_name}-alb-sg"
  }
}

