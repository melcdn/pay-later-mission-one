locals {
  my_public_ip = "${chomp(data.http.my_ip.body)}/32"
}


# Get current region
data "aws_region" "current" {}

# Query the ami_id of Amazon Linux 2
data "aws_ami" "amazon_linux2_latest" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm*"]
  }
}

data "aws_ami" "amazon_linux_nat_latest" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn-ami-vpc-nat-hvm*"]
  }
}

# Query available AZs
data "aws_availability_zones" "available_azs" {
  state = "available"

}

# Query for the required role for spot fleet tagging
data "aws_iam_role" "ec2_fleet_tagging_role" {
  name = "aws-ec2-spot-fleet-tagging-role"
}

data "http" "my_ip" {
  url = "http://ipv4.icanhazip.com"
}


