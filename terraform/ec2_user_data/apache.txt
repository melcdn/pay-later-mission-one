#!/bin/bash
yum update –y
yum install telnet -y
yum -y install httpd
systemctl start httpd 
systemctl enable httpd
mkdir /var/www/html/apache/
echo "<h2><u>Apache Web Server</u></h2><h3>Hostname: $(hostname -f)</h3>" > /var/www/html/apache/index.html