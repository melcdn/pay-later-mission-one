# Create launch template for Nginx EC2 instances
resource "aws_launch_template" "nginx_lt" {
  name                   = "${var.client_name}-nginx-lt"
  description            = "Launch template for EC2 nginx fleet spot"
  image_id               = data.aws_ami.amazon_linux2_latest.id
  instance_type          = var.instance_type
  key_name               = var.key_pair_name
  vpc_security_group_ids = [aws_security_group.web_server_sg.id]
  user_data              = filebase64(var.nginx_user_data_location)

  instance_market_options {
    market_type = "spot"
  }
}
# Create auto-scaling group for Nginx EC2 instances
resource "aws_autoscaling_group" "nginx_asg" {
  name                = "${var.client_name}-nginx_sg"
  vpc_zone_identifier = aws_subnet.private_subnet.*.id
  target_group_arns   = [aws_lb_target_group.nginx_tg.arn]
  health_check_type   = "EC2"

  min_size         = 0
  max_size         = 2
  desired_capacity = 2

  launch_template {
    id      = aws_launch_template.nginx_lt.id
    version = "$Latest"
  }

  tag {
    key                 = "Name"
    value               = "${var.client_name}-nginx-web-server"
    propagate_at_launch = true
  }
}

resource "aws_autoscaling_schedule" "nginx_scale_in" {
  scheduled_action_name  = "${var.client_name}-nginx-scale-in"
  min_size               = -1
  max_size               = -1
  desired_capacity       = 0
  recurrence             = "0 9 * * *" # 5PM Manila Time
  autoscaling_group_name = aws_autoscaling_group.nginx_asg.name
}

resource "aws_autoscaling_schedule" "nginx_scale_out" {
  scheduled_action_name  = "${var.client_name}-nginx-scale-out"
  min_size               = -1
  max_size               = -1
  desired_capacity       = 2
  recurrence             = "0 12 * * *" # 8AM Manila Time
  autoscaling_group_name = aws_autoscaling_group.nginx_asg.name
}