# Create bastion EC2 instance (will also serve as Nat server)
resource "aws_instance" "bastion" {
  ami           = data.aws_ami.amazon_linux_nat_latest.id
  instance_type = var.instance_type
  key_name      = var.key_pair_name
  subnet_id     = element(aws_subnet.public_subnet.*.id, 0)
  # aws_subnet.public_subnet[0].id
  vpc_security_group_ids      = [aws_security_group.bastion_sg.id]
  user_data                   = filebase64(var.bastion_user_data_location)
  source_dest_check           = false # will act as nat server
  associate_public_ip_address = true
  tags = {
    Name = "${var.client_name}-bastion-server"
  }
}

# Create bastion security group
resource "aws_security_group" "bastion_sg" {
  name   = "${var.client_name}-bastion-sg"
  vpc_id = aws_vpc.main.id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = var.private_subnet_cidrs
  }
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = var.private_subnet_cidrs
  }
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [local.my_public_ip]
    description = "Use only my public ip for SSH access"
  }
  ingress {
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [var.vpc_cidr]
  }
  egress {
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "${var.client_name}-bastion-sg"
  }
}

