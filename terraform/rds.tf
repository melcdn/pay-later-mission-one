# Create db subnet group
resource "aws_db_subnet_group" "db_subnet_group" {
  name       = "${var.client_name}-db-subnet-group"
  subnet_ids = aws_subnet.private_subnet.*.id

  tags = {
    Name = "${var.client_name}-db-subnet-group"
  }
}

# Create postgresql db on private-subnet1
resource "aws_db_instance" "rds_main" {
  identifier                 = "${var.client_name}-postgresql-main"
  allocated_storage          = 20
  db_subnet_group_name       = aws_db_subnet_group.db_subnet_group.name
  availability_zone          = element(data.aws_availability_zones.available_azs.names, 0)
  vpc_security_group_ids     = [aws_security_group.rds_sg.id]
  engine                     = "postgres"
  engine_version             = "12"
  instance_class             = "db.t2.micro"
  name                       = "${var.client_name}_postgresql_main"
  username                   = "postgres"
  password                   = "postgres"
  auto_minor_version_upgrade = true
  skip_final_snapshot        = true
  tags = {
    Name = "${var.client_name}-postgresql-main"
  }

}

# Create postgresql db on private-subnet2
resource "aws_db_instance" "rds_standby" {
  identifier                 = "${var.client_name}-postgresql-standby"
  allocated_storage          = 20
  db_subnet_group_name       = aws_db_subnet_group.db_subnet_group.name
  availability_zone          = element(data.aws_availability_zones.available_azs.names, 1)
  vpc_security_group_ids     = [aws_security_group.rds_sg.id]
  engine                     = "postgres"
  engine_version             = "12"
  instance_class             = "db.t2.micro"
  name                       = "${var.client_name}_postgresql_standby"
  username                   = "postgres"
  password                   = "postgres"
  auto_minor_version_upgrade = true
  skip_final_snapshot        = true



  tags = {
    Name = "${var.client_name}-postgresql-standby"
  }

}