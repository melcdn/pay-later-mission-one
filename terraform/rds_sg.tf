# Create the security group of the ALB
resource "aws_security_group" "rds_sg" {
  name   = "${var.client_name}-rds-sg"
  vpc_id = aws_vpc.main.id

  ingress {
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = var.private_subnet_cidrs
    description = "Allow db port from private-subnets"
  }

  ingress {
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = [var.public_subnet_cidrs[0]]
    description = "Allow db port from public-subnet-1 (bastion host)"
  }

  tags = {
    Name = "${var.client_name}-rds-sg"
  }
}

